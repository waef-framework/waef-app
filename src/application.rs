use std::{cmp::max, sync::mpsc::Sender, thread};

use waef_core::{
    core::{ExecutionResult, Message, WaefError},
    IsMessage,
};

use crate::OnAppStarted;

pub struct ApplicationBuilder {
    dispatcher: Sender<Message>,
    plugins: Vec<Box<dyn ApplicationPlugin>>,
}

pub type ApplicationPluginTick = Box<dyn FnMut() -> Result<ExecutionResult, WaefError>>;

pub type ApplicationPluginMainThread = Box<
    dyn FnOnce(Box<dyn FnMut() -> ExecutionResult>, Box<dyn FnOnce()>) -> Result<(), WaefError>,
>;

pub struct Application {
    dispatcher: Sender<Message>,
    on_tick_callbacks: Vec<ApplicationPluginTick>,
    main_thread: ApplicationPluginMainThread,
    on_cleanup_callbacks: Vec<Box<dyn FnOnce() -> Result<(), WaefError>>>,
}
impl Application {
    pub fn dispatcher(&self) -> Sender<Message> {
        self.dispatcher.clone()
    }

    pub fn new(dispatcher: Sender<Message>) -> Self {
        Self {
            dispatcher,
            main_thread: Box::new(|mut on_update, on_close| {
                let mut result = ExecutionResult::NoOp;
                while result != ExecutionResult::Kill {
                    result = on_update();
                    if result == ExecutionResult::NoOp {
                        thread::yield_now();
                    }
                }
                on_close();
                Ok(())
            }),
            on_tick_callbacks: vec![],
            on_cleanup_callbacks: vec![],
        }
    }

    pub fn with_main_thread(
        &mut self,
        thread: impl FnOnce(Box<dyn FnMut() -> ExecutionResult>, Box<dyn FnOnce()>) -> Result<(), WaefError>
            + 'static,
    ) {
        self.main_thread = Box::new(thread);
    }

    pub fn with_cleanup(&mut self, cleanup: impl FnOnce() -> Result<(), WaefError> + 'static) {
        self.on_cleanup_callbacks.push(Box::new(cleanup));
    }

    pub fn with_on_tick(
        &mut self,
        on_tick: impl FnMut() -> Result<ExecutionResult, WaefError> + 'static,
    ) {
        self.on_tick_callbacks.push(Box::new(on_tick))
    }

    pub fn run(self) -> Result<(), WaefError> {
        tracing::event!(tracing::Level::TRACE, message = "Application::main_thread");

        let mut on_ticks = self.on_tick_callbacks;
        (self.main_thread)(
            Box::new(move || {
                let _guard = tracing::trace_span!("Application::main_thread::tick").entered();

                on_ticks
                    .iter_mut()
                    .fold(ExecutionResult::NoOp, |result, cb| match cb() {
                        Ok(res) => max(result, res),
                        Err(err) => {
                            tracing::error!("Error during update: {:?}", err);
                            max(ExecutionResult::Kill, result)
                        }
                    })
            }),
            Box::new(move || {
                let _guard = tracing::trace_span!("Application::main_thread::clean_up").entered();

                for cb in self.on_cleanup_callbacks {
                    _ = cb();
                }
            }),
        )
    }
}

pub trait ApplicationPlugin {
    fn apply(self: Box<Self>, app: &mut Application);
}

impl ApplicationBuilder {
    pub fn new(dispatcher: Sender<Message>) -> Self {
        Self {
            dispatcher,
            plugins: vec![],
        }
    }

    pub fn dispatcher(&self) -> Sender<Message> {
        self.dispatcher.clone()
    }

    pub fn use_plugin(mut self, plugin: impl ApplicationPlugin + 'static) -> Self {
        self.plugins.push(Box::new(plugin));
        self
    }

    pub fn run(self) -> Result<(), WaefError> {
        self.run_with(|cb| cb())
    }

    pub fn run_with(
        mut self,
        callback: impl FnOnce(Box<dyn FnOnce() -> Result<(), WaefError>>) -> Result<(), WaefError>,
    ) -> Result<(), WaefError> {
        let tokio = tokio::runtime::Builder::new_current_thread()
            .build()
            .map_err(WaefError::new)?;
        let _tokioguard = tokio.enter();

        let invoke = Box::new(move || {
            tracing::event!(tracing::Level::TRACE, message = "Application::start");

            let mut application = Application::new(self.dispatcher.clone());

            self.apply_plugins_to(&mut application);
            self.pre_application_run();
            application.run()
        });
        callback(invoke)
    }

    fn apply_plugins_to(&mut self, application: &mut Application) {
        let _apply_guard = tracing::trace_span!(
            "Application::apply_plugins",
            plugin_count = self.plugins.len()
        )
        .entered();

        for plugin in self.plugins.drain(..) {
            plugin.apply(application);
        }
    }

    fn pre_application_run(&self) {
        _ = self.dispatcher.send(OnAppStarted {}.into_message());
    }
}
