use std::{
    cell::RefCell,
    cmp::max,
    rc::Rc,
    sync::mpsc::{Receiver, Sender},
    time::Duration,
};

use waef_core::{
    actors::{ActorsInjector, ActorsPlugin, IsActor, UseActors},
    core::{CompiledDispatchFilter, ExecutionResult, Message},
    timers::{IsTimer, TimersPlugin, UseTimers},
};

use crate::{Application, ApplicationPlugin};

#[cfg(feature = "sys")]
fn create_get_time_from_start_callback() -> Box<dyn Fn() -> Duration> {
    let start_time = std::time::Instant::now();

    Box::new(move || std::time::Instant::now().duration_since(start_time))
}

#[cfg(feature = "js")]
fn create_get_time_from_start_callback() -> Box<dyn Fn() -> Duration> {
    Box::new(move || {
        Duration::from_secs_f64(web_sys::window().unwrap().performance().unwrap().now() / 1000.0)
    })
}

pub struct UseTimersPlugin {
    get_duration_since_start: Box<dyn Fn() -> Duration>,
    timers: Vec<Box<dyn IsTimer>>,
}
impl UseTimersPlugin {
    pub fn new() -> Self {
        Self {
            get_duration_since_start: create_get_time_from_start_callback(),
            timers: vec![],
        }
    }

    pub fn with_get_duration_since_start(
        get_current_time: impl Fn() -> Duration + 'static,
    ) -> Self {
        Self {
            get_duration_since_start: Box::new(get_current_time),
            timers: vec![],
        }
    }
}
impl UseTimersPlugin {
    pub fn use_plugin(self, plugin: impl TimersPlugin) -> UseTimersPlugin {
        plugin.apply(self)
    }
}
impl UseTimers for UseTimersPlugin {
    fn use_timer(mut self, timer: Box<dyn IsTimer>) -> Self {
        self.timers.push(timer);
        self
    }
}
impl ApplicationPlugin for UseTimersPlugin {
    fn apply(self: Box<Self>, app: &mut Application) {
        let get_current_time = self.get_duration_since_start;

        let start_time = get_current_time();
        let mut tickers = self
            .timers
            .into_iter()
            .map(|timer| (start_time + timer.duration(), timer))
            .collect::<Vec<_>>();

        app.with_on_tick(move || {
            let current_time = get_current_time();

            let result = tickers.iter_mut().fold(
                ExecutionResult::NoOp,
                |result, (time_of_next_tick, timer)| {
                    let tick_duration = timer.duration();

                    let mut factor = 0;
                    while *time_of_next_tick <= current_time {
                        factor = factor + 1;
                        *time_of_next_tick += tick_duration;
                        if factor > 1000 {
                            *time_of_next_tick = current_time + tick_duration;
                            break;
                        }
                    }

                    if factor > 0 {
                        timer.tick(tick_duration * factor);
                        max(result, ExecutionResult::Processed)
                    } else {
                        max(result, ExecutionResult::NoOp)
                    }
                },
            );
            Ok(result)
        })
    }
}

struct TimerInstance {
    timer: Box<dyn IsTimer>,
    next_update: Duration,
}
impl TimerInstance {
    pub fn new(timer: Box<dyn IsTimer>) -> Self {
        Self {
            next_update: timer.duration(),
            timer,
        }
    }
}

pub struct UseActorsPlugin {
    get_duration_since_start: Box<dyn Fn() -> Duration>,
    actors: Vec<(CompiledDispatchFilter, Box<dyn IsActor>)>,
    plugins: Vec<Box<dyn FnOnce(Box<Self>) -> Box<Self>>>,
    receiver: Receiver<Message>,
    timers: Vec<Box<dyn Fn(Sender<Message>) -> Box<dyn IsTimer>>>,
}
impl UseActorsPlugin {
    pub fn new(receiver: Receiver<Message>) -> Self {
        Self {
            get_duration_since_start: create_get_time_from_start_callback(),
            actors: vec![],
            plugins: vec![],
            receiver,
            timers: vec![],
        }
    }
    pub fn with_io_thread_count(self, _io_thread_count: usize) -> Self {
        self
    }

    pub fn try_with_io_thread_count(self, _io_thread_count: Option<usize>) -> Self {
        self
    }

    pub fn with_processing_thread_count(self, _processing_thread_count: usize) -> Self {
        self
    }

    pub fn try_with_processing_thread_count(self, _processing_thread_count: Option<usize>) -> Self {
        self
    }

    pub fn with_priority_thread_count(self, _priority_thread_count: usize) -> Self {
        self
    }

    pub fn try_with_priority_thread_count(self, _priority_thread_count: Option<usize>) -> Self {
        self
    }

    pub fn with_timer(
        mut self,
        factory: impl Fn(Sender<Message>) -> Box<dyn IsTimer> + 'static,
    ) -> Self {
        self.timers.push(Box::new(factory));
        self
    }
}
impl ApplicationPlugin for UseActorsPlugin {
    fn apply(mut self: Box<Self>, app: &mut Application) {
        self = self
            .plugins
            .drain(..)
            .collect::<Vec<_>>()
            .into_iter()
            .fold(self, |this, plugin| plugin(this));

        let mut local_timers = self
            .timers
            .iter()
            .map(|factory| TimerInstance::new(factory(app.dispatcher().clone())))
            .collect::<Vec<_>>();

        let local_plugin = Rc::new(RefCell::new(self));
        let plugin = local_plugin.clone();

        app.with_on_tick(Box::new(move || {
            let mut plugin = plugin.borrow_mut();
            let mut result = ExecutionResult::NoOp;
            loop {
                if let Ok(msg) = plugin.receiver.try_recv() {
                    let _guard = tracing::trace_span!(
                        "use_actors::dispatch",
                        message_name = msg.name,
                        message_len = msg.buffer.len()
                    )
                    .entered();
                    for (filter, actor) in plugin.actors.iter_mut() {
                        if filter.allows(&msg) {
                            let _guard = tracing::trace_span!(
                                "use_actors::dispatch::actor",
                                actor_name = actor.name(),
                                message_name = msg.name,
                                message_len = msg.buffer.len()
                            )
                            .entered();
                            result = max(result, actor.dispatch(&msg));
                        }
                    }
                } else {
                    let now = (plugin.get_duration_since_start)();

                    local_timers.iter_mut().for_each(|timer| {
                        let mut total_tick = Duration::ZERO;
                        while now >= timer.next_update {
                            let duration = timer.timer.duration();
                            total_tick += duration;
                            timer.next_update += duration;
                        }
                        if !total_tick.is_zero() {
                            timer.timer.tick(total_tick);
                        }
                    });
                    break;
                }
            }
            Ok(result)
        }));

        let plugin = local_plugin.clone();
        app.with_cleanup(move || {
            let mut plugin = plugin.borrow_mut();
            for (_, actor) in plugin.actors.iter_mut() {
                actor.dispose()
            }
            Ok(())
        });
    }
}
impl UseActors for UseActorsPlugin {
    fn use_actor(mut self, actor: Box<dyn IsActor>) -> Self {
        self.actors.push((actor.filter().compile(), actor));
        self
    }
}
impl UseActors for Box<UseActorsPlugin> {
    fn use_actor(mut self, actor: Box<dyn IsActor>) -> Self {
        self.actors.push((actor.filter().compile(), actor));
        self
    }
}
impl UseActorsPlugin {
    pub fn use_plugin(mut self, plugin: impl ActorsPlugin + 'static) -> UseActorsPlugin {
        self.plugins.push(Box::new(|this| plugin.apply(this)));
        self
    }

    pub fn use_inject(self, plugin: &impl ActorsInjector) -> UseActorsPlugin {
        plugin.inject(self)
    }
}
