use std::cell::RefCell;
use std::collections::HashMap;

use waef_core::resources::ResourceStore;

pub struct LockingResourceStore {
    data: RefCell<HashMap<u128, Vec<u8>>>,
}
unsafe impl Send for LockingResourceStore {}
unsafe impl Sync for LockingResourceStore {}

impl LockingResourceStore {
    pub fn new() -> Self {
        Self {
            data: RefCell::new(HashMap::new()),
        }
    }
}
impl ResourceStore for LockingResourceStore {
    fn get_size(&self, key: u128) -> Option<usize> {
        self.data.borrow_mut().get(&key).map(|d| d.len())
    }

    fn remove(&self, key: u128) -> Option<Box<[u8]>> {
        self.data
            .borrow_mut()
            .remove(&key)
            .map(|d| d.into_boxed_slice())
    }

    fn put(&self, key: u128, data: &[u8]) {
        let mut store = self.data.borrow_mut();
        if let Some(buffer) = store.get_mut(&key) {
            let target_buffer_size = data.len();
            if buffer.len() != target_buffer_size {
                buffer.resize(target_buffer_size, 0);
            }
            buffer.copy_from_slice(data);
        } else {
            let mut item = vec![0u8; data.len()];
            item.copy_from_slice(data);
            store.insert(key, item);
        }
    }

    fn get(&self, key: u128, buffer: &mut [u8]) -> Option<usize> {
        self.get_slice(key, 0, buffer)
    }

    fn put_slice(&self, key: u128, start_index: usize, data: &[u8]) {
        if let Some(buffer) = self.data.borrow_mut().get_mut(&key) {
            let target_buffer_size = start_index + data.len();
            if buffer.len() < target_buffer_size {
                buffer.extend(vec![0u8; target_buffer_size - buffer.len()]);
            }
            buffer[start_index..target_buffer_size].copy_from_slice(data);
        } else {
            let mut item = vec![0u8; start_index + data.len()];
            item[start_index..start_index + data.len()].copy_from_slice(data);
            self.data.borrow_mut().insert(key, item);
        }
    }

    fn get_slice(&self, key: u128, start_index: usize, buffer: &mut [u8]) -> Option<usize> {
        if let Some(data) = self.data.borrow_mut().get(&key) {
            let target_data_end = start_index + data.len();
            let target_data_range = if data.len() < target_data_end {
                start_index..data.len()
            } else {
                start_index..target_data_end
            };
            let read_len = target_data_range.len();
            buffer.copy_from_slice(&data[target_data_range]);
            Some(read_len)
        } else {
            None
        }
    }

    fn get_cloned(&self, key: u128) -> Option<Vec<u8>> {
        if let Some(data) = self.data.borrow_mut().get(&key) {
            Some(data.clone())
        } else {
            None
        }
    }
}
