#[cfg(any(feature = "js", feature = "sys"))]
mod application;

#[cfg(any(feature = "js", feature = "sys"))]
pub use application::Application;

#[cfg(any(feature = "js", feature = "sys"))]
pub use application::ApplicationBuilder;

#[cfg(any(feature = "js", feature = "sys"))]
pub use application::ApplicationPlugin;

#[cfg(all(feature = "js", feature = "multithreaded"))]
compile_error!("Multithreaded js is not currently supportted");

#[cfg(all(feature = "multithreaded", any(feature = "js", feature = "sys")))]
mod mt;

#[cfg(all(feature = "multithreaded", any(feature = "js", feature = "sys")))]
pub use mt::*;

#[cfg(all(not(feature = "multithreaded"), any(feature = "js", feature = "sys")))]
mod st;

#[cfg(all(not(feature = "multithreaded"), any(feature = "js", feature = "sys")))]
pub use st::*;

use waef_core::message_pod;

#[message_pod("app:on_start")]
pub struct OnAppStarted {}
