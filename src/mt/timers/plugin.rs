use std::sync::mpsc::{channel, Receiver, Sender};

use waef_core::{
    core::{ExecutionResult, WaefError},
    timers::{IsTimer, TimersPlugin, UseTimers},
};

use crate::application::{Application, ApplicationPlugin};

use super::runtime::TimerRuntimeBuilder;

pub struct UseTimersPlugin {
    timers: Vec<Box<dyn IsTimer>>,
    terminate: (Sender<()>, Receiver<()>),
}
impl Default for UseTimersPlugin {
    fn default() -> Self {
        Self::new()
    }
}
impl UseTimersPlugin {
    pub fn new() -> Self {
        Self {
            timers: vec![],
            terminate: channel(),
        }
    }
}
impl UseTimers for UseTimersPlugin {
    fn use_timer(mut self, timer: Box<dyn IsTimer>) -> Self {
        self.timers.push(timer);
        self
    }
}

impl ApplicationPlugin for UseTimersPlugin {
    fn apply(self: Box<Self>, app: &mut Application) {
        let mut timer_builder = TimerRuntimeBuilder::new();
        for timer in self.timers {
            timer_builder = timer_builder.add_timer_box(timer);
        }

        let thread_runtime = timer_builder.start(self.terminate.0);

        let app_terminate = self.terminate.1;
        app.with_on_tick(move || {
            if let Ok(()) = app_terminate.try_recv() {
                Ok(ExecutionResult::Kill)
            } else {
                Ok(ExecutionResult::NoOp)
            }
        });
        app.with_cleanup(move || {
            let _span = tracing::trace_span!("use_timers::clean_up").entered();

            _ = thread_runtime.0.send(());

            match thread_runtime.1.join() {
                Ok(result) => result,
                Err(_) => Err(WaefError::new("Failed to join onto actor runtime thread")),
            }
        });
    }
}

impl UseTimersPlugin {
    pub fn use_plugin(self, plugin: impl TimersPlugin) -> UseTimersPlugin {
        plugin.apply(self)
    }
}
