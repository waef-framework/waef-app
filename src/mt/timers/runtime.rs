use std::{
    sync::mpsc::{channel, Sender, TryRecvError},
    thread::{self, JoinHandle},
    time::{Duration, Instant},
};
use waef_core::{core::WaefError, timers::IsTimer};

struct TimerInstance {
    timer: Box<dyn IsTimer>,
    next_update: Instant,
}

pub struct TimerRuntimeBuilder {
    timers: Vec<TimerInstance>,
}
impl Default for TimerRuntimeBuilder {
    fn default() -> Self {
        Self::new()
    }
}
impl TimerRuntimeBuilder {
    pub fn new() -> Self {
        Self { timers: Vec::new() }
    }

    pub fn add_timer_box(mut self, timer: Box<dyn IsTimer>) -> Self {
        let duration = timer.duration();
        self.timers.push(TimerInstance {
            timer,
            next_update: Instant::now() + duration,
        });
        self
    }
}

impl TimerRuntimeBuilder {
    pub fn start(
        mut self,
        report_terminate: Sender<()>,
    ) -> (Sender<()>, JoinHandle<Result<(), WaefError>>) {
        let (tell_terminate, on_terminate) = channel();
        if self.timers.is_empty() {
            return (
                tell_terminate,
                thread::Builder::new()
                    .name("global_timer_thread".to_owned())
                    .spawn(|| Ok(()))
                    .unwrap(),
            );
        }

        (
            tell_terminate,
            thread::Builder::new()
                .name("global_timer_thread".to_owned())
                .spawn(move || {
                    let tokio = tokio::runtime::Builder::new_current_thread()
                        .build()
                        .map_err(WaefError::new)?;
                    let _tokioguard = tokio.enter();

                    let mut tick = || {
                        let now = Instant::now();

                        self.timers.iter_mut().for_each(|t| {
                            let mut factor = 0;
                            while t.next_update <= now {
                                t.next_update += t.timer.duration();
                                factor += 1;
                            }
                            if factor > 0 {
                                let _guard = tracing::trace_span!(
                                    "timer_tick",
                                    name = t.timer.name(),
                                    duration_ms = t.timer.duration().as_millis()
                                )
                                .entered();

                                t.timer.tick(t.timer.duration() * factor);
                            }
                        });
                        match self
                            .timers
                            .iter()
                            .min_by(|a, b| a.next_update.cmp(&b.next_update))
                        {
                            None => thread::yield_now(),
                            Some(timer) => {
                                let next_update_in = timer.next_update - now;
                                if next_update_in < Duration::from_millis(32) {
                                    thread::yield_now();
                                } else {
                                    thread::sleep(next_update_in / 2);
                                }
                            }
                        }

                        match on_terminate.try_recv() {
                            Ok(_) => Ok(false),
                            Err(TryRecvError::Empty) => Ok(true),
                            Err(TryRecvError::Disconnected) => Err(WaefError::new(
                                "Timer Runtime on_terminate receiver disconnected",
                            )),
                        }
                    };
                    let mut error = None;
                    loop {
                        match tick() {
                            Ok(true) => {}
                            Ok(false) => break,
                            Err(err) => {
                                error = Some(err);
                                break;
                            }
                        }
                    }
                    for timer in self.timers.iter_mut() {
                        timer.timer.dispose();
                    }
                    _ = report_terminate.send(());
                    match error {
                        None => Ok(()),
                        Some(error) => Err(error),
                    }
                })
                .unwrap(),
        )
    }
}
