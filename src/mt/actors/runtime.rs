use std::{
    collections::HashMap,
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc, Barrier,
    },
    thread::{self, JoinHandle},
    time::{Duration, Instant},
};

use super::thread::ActorThread;
use waef_core::{
    actors::{collection::ActorCollectionBuilder, ActorAlignment, IsActor},
    core::{Message, WaefError},
    IsDispatcher, IsTimer,
};

fn forward_receive_to_senders<
    T: Send + 'static,
    U: Send + 'static + Clone,
    TMapper: Send + 'static + Fn(T) -> U,
>(
    receiver: Receiver<T>,
    senders: Vec<Sender<U>>,
    mapper: TMapper,
) -> JoinHandle<()> {
    thread::Builder::new()
        .name("forward_receive_to_senders".to_owned())
        .spawn(move || {
            receiver.into_iter().for_each(|msg| {
                let msg = mapper(msg);
                senders
                    .iter()
                    .for_each(|sender| _ = sender.send(msg.clone()));
            });
        })
        .unwrap()
}

struct TimerInstance {
    timer: Box<dyn IsTimer>,
    next_update: Instant,
}
impl TimerInstance {
    pub fn new(instance: Box<dyn IsTimer>) -> Self {
        Self {
            next_update: Instant::now() + instance.duration(),
            timer: instance,
        }
    }
}

#[derive(Default)]
pub struct ActorRuntimeBuilder {
    actors: Vec<Box<dyn IsActor>>,
}

impl ActorRuntimeBuilder {
    pub fn new() -> Self {
        Self { actors: Vec::new() }
    }

    pub fn add_box(mut self, actor: Box<dyn IsActor>) -> Self {
        self.actors.push(actor);
        self
    }
}

impl ActorRuntimeBuilder {
    pub fn start(
        self,
        priority_thread_count: usize,
        processing_thread_count: usize,
        io_thread_count: usize,
        on_dispatch: Receiver<Message>,
        do_terminate: Sender<()>,
        timers: Vec<Box<dyn Fn(Sender<Message>) -> Box<dyn IsTimer>>>,
    ) -> (Sender<()>, JoinHandle<Result<(), WaefError>>) {
        let _init_guard = tracing::trace_span!(
            "actor_runtime::initialize",
            priority_thread_count,
            processing_thread_count,
            io_thread_count
        )
        .entered();

        let actors = self.actors;

        let mut thread_builders = HashMap::new();
        _ = thread_builders.insert(
            ActorAlignment::Processing,
            ActorCollectionBuilder::new_vec(ActorAlignment::Processing, processing_thread_count),
        );
        _ = thread_builders.insert(
            ActorAlignment::IO,
            ActorCollectionBuilder::new_vec(ActorAlignment::IO, io_thread_count),
        );
        _ = thread_builders.insert(
            ActorAlignment::Priority,
            ActorCollectionBuilder::new_vec(ActorAlignment::Priority, priority_thread_count),
        );

        for actor in actors.into_iter() {
            if let Some(thread_builder) = thread_builders
                .get_mut(&actor.alignment())
                .and_then(|thread_builders| thread_builders.iter_mut().min_by_key(|f| f.weight()))
            {
                thread_builder.add_box_mut(actor);
                thread_builder.add_weight(1); // even if the actors weight is 0 this will encourage spreading across threads
            }
        }

        let (tell_on_terminated, on_terminated) = channel();
        let (tell_terminate, on_terminate_requested) = channel();
        let threads = thread_builders
            .into_values()
            .into_iter()
            .flatten()
            .filter(|thread| !thread.is_empty())
            .collect::<Vec<_>>();

        if threads.is_empty() {
            return (
                tell_on_terminated,
                thread::Builder::new()
                    .name("actors={}".to_owned())
                    .spawn(|| Ok(()))
                    .unwrap(),
            );
        }

        let barrier = Arc::new(Barrier::new(threads.len() + 1));
        let threads = threads
            .into_iter()
            .map(|thread| {
                let actor = thread.build();

                let (timer_sender, timer_receiver) = channel::<Message>();
                let mut local_timers = timers
                    .iter()
                    .map(|factory| TimerInstance::new(factory(timer_sender.clone())))
                    .filter(|timer| actor.filter().contains_any(timer.timer.outputs()))
                    .collect::<Vec<_>>();

                let additional_receivers = if local_timers.len() > 0 {
                    vec![timer_receiver]
                } else {
                    vec![]
                };

                let do_processing_terminate = tell_terminate.clone();
                ActorThread::spawn_thread(
                    barrier.clone(),
                    actor,
                    move || -> Instant {
                        let now = Instant::now();

                        let mut min_timeout = Instant::now() + Duration::from_millis(250);
                        local_timers.iter_mut().for_each(|timer| {
                            let mut total_tick = Duration::ZERO;
                            while timer.next_update <= now {
                                let duration = timer.timer.duration();
                                total_tick += duration;
                                timer.next_update += duration;
                            }
                            if !total_tick.is_zero() {
                                timer.timer.tick(total_tick);
                            }
                            min_timeout = min_timeout.min(timer.next_update);
                        });
                        min_timeout
                    },
                    move || _ = do_processing_terminate.send(()),
                    additional_receivers,
                )
            })
            .collect::<Vec<ActorThread>>();

        forward_receive_to_senders(
            on_dispatch,
            threads
                .iter()
                .map(|thread| thread.sender().clone())
                .collect(),
            |m| Arc::new(m),
        );
        forward_receive_to_senders(on_terminated, vec![tell_terminate], |()| ());

        let runtime_thread = thread::Builder::new()
            .name("actor_runtime".to_owned())
            .spawn(move || {
                let wait_for_terminate = || match on_terminate_requested.recv() {
                    Ok(_) => {
                        tracing::event!(tracing::Level::TRACE, message = "terminate received");
                        Ok(())
                    }
                    Err(err) => {
                        tracing::error!("Failed to receive message: {:?}", err);
                        Err(WaefError::new(format!(
                            "Failed to receive message: {:?}",
                            err
                        )))
                    }
                };

                let failure = match wait_for_terminate() {
                    Ok(()) => None,
                    Err(err) => Some(err),
                };

                let cleanup = || {
                    let _span = tracing::trace_span!("actor_runtime::cleanup").entered();
                    tracing::info!(
                        "Actor Runtime received kill notification, terminating actor threads..."
                    );

                    threads.iter().for_each(|s| s.kill());
                    let mut errs = Vec::new();
                    for thread in threads {
                        let _span = tracing::trace_span!(
                            "actor_runtime::cleanup::join",
                            actor = thread.name()
                        )
                        .entered();

                        match thread.join() {
                            Ok(()) => (),
                            Err(err) => errs.push(err),
                        };
                    }

                    _ = do_terminate.send(());
                    if errs.is_empty() {
                        Ok(())
                    } else {
                        Err(WaefError::combine_unchecked(&errs[..]))
                    }
                };
                match (failure, cleanup()) {
                    (None, rhs) => rhs,
                    (Some(err), Ok(())) => Err(err),
                    (Some(lhs), Err(rhs)) => Err(WaefError::merge(lhs, rhs)),
                }
            })
            .unwrap();

        barrier.wait();
        (tell_on_terminated, runtime_thread)
    }
}
