use std::sync::mpsc::{channel, Receiver, RecvTimeoutError, Sender, TryRecvError};
use std::sync::{Arc, Barrier};
use std::thread::{self, JoinHandle};
use std::time::{Duration, Instant};

use waef_core::actors::{collection::ActorCollection, IsActor};
use waef_core::core::{
    CompiledDispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, Message, Result, WaefError
};
use waef_core::{ActorAlignment, HasActorAlignment};

pub struct ActorThread {
    name: String,
    handle: JoinHandle<()>,
    sender: Sender<Arc<Message>>,
    killer: Sender<()>,
}

impl ActorThread {
    pub fn name(&self) -> &String {
        &self.name
    }

    fn run_collection(
        mut actor: ActorCollection,
        filter: CompiledDispatchFilter,
        kill_receiver: Receiver<()>,
        mut on_tick_callback: impl FnMut() -> Instant + Send + 'static,
        mut on_kill_callback: impl FnMut() + Send + 'static,
        dispatch_sender: Sender<Arc<Message>>,
        dispatch_receiver: Receiver<Arc<Message>>,
        mut additional_receivers: Vec<Receiver<Message>>,
    ) {
        let alignment = match actor.alignment() {
            ActorAlignment::Processing => "processing",
            ActorAlignment::IO => "io",
            ActorAlignment::Priority => "priority",
        };

        tracing::event!(
            tracing::Level::TRACE,
            message = "start actor_thread",
            alignment = alignment,
            actor_count = actor.len(),
            name = actor.name(),
            weight = actor.weight()
        );

        let mut last_status = ExecutionResult::NoOp;

        let mut wait_until = on_tick_callback();
        while last_status != ExecutionResult::Kill {
            match kill_receiver.try_recv() {
                Ok(()) => {
                    last_status = last_status.max(ExecutionResult::Kill);
                }
                Err(TryRecvError::Empty) => {
                    while last_status != ExecutionResult::Kill {
                        match recv_timeout_hires(&dispatch_receiver, wait_until - Instant::now()) {
                            Ok(msg) => {
                                if filter.allows(&msg) {
                                    let _guard = tracing::trace_span!(
                                        "actor_thread::dispatch",
                                        actors = actor.name(),
                                        message_name = msg.name,
                                        message_len = msg.buffer.len()
                                    )
                                    .entered();

                                    match actor.dispatch(&msg) {
                                        ExecutionResult::NoOp => {
                                            last_status = last_status.max(ExecutionResult::NoOp)
                                        }
                                        ExecutionResult::Processed => {
                                            last_status =
                                                last_status.max(ExecutionResult::Processed);
                                        }
                                        ExecutionResult::Kill => {
                                            on_kill_callback();
                                            last_status = last_status.max(ExecutionResult::Kill);
                                        }
                                    }
                                }
                            }
                            Err(RecvTimeoutError::Timeout) => {
                                if kill_receiver.try_recv().is_ok() {
                                    last_status = last_status.max(ExecutionResult::Kill);
                                    break;
                                }

                                wait_until = on_tick_callback();
                                for rec in additional_receivers.iter_mut() {
                                    while last_status != ExecutionResult::Kill {
                                        match rec.try_recv() {
                                            Ok(msg) => {
                                                _ = dispatch_sender.send(Arc::new(msg));
                                            }
                                            Err(TryRecvError::Empty) => {
                                                break;
                                            }
                                            Err(err) => {
                                                tracing::error!(
                                                    "Unexpected error when receiving dispatch of message from additional receivers: {:?} (actors: {:?})",
                                                    err,
                                                    actor.name()
                                                );

                                                let _guard = tracing::trace_span!(
                                                    "actor_thread::kill",
                                                    actors = actor.name()
                                                )
                                                .entered();
                                                on_kill_callback();
                                                last_status =
                                                    last_status.max(ExecutionResult::Kill);
                                                break;
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                            Err(err) => {
                                tracing::error!(
                                    "Unexpected error when receiving dispatch of message from main receiver: {:?} (actors: {:?})",
                                    err,
                                    actor.name()
                                );
                                on_kill_callback();
                                last_status = last_status.max(ExecutionResult::Kill);
                                break;
                            }
                        }
                    }

                    match last_status {
                        ExecutionResult::NoOp => std::thread::yield_now(),
                        ExecutionResult::Processed => last_status = ExecutionResult::NoOp,
                        ExecutionResult::Kill => (),
                    }
                }
                Err(err) => {
                    tracing::error!(
                        "Unexpected error when receiving dispatch of kill command:{:?} (actors: {:?})",
                        err,
                        actor.name()
                    );
                    on_kill_callback();
                    last_status = last_status.max(ExecutionResult::Kill);
                }
            };
        }
        actor.dispose();
        tracing::info!(
            "End of actor thread containing {:?} actors: {:?}",
            actor.len(),
            actor.name()
        );
    }

    pub fn spawn_thread(
        barrier: Arc<Barrier>,
        collection: ActorCollection,
        on_tick_callback: impl FnMut() -> Instant + Send + 'static,
        on_kill_callback: impl FnMut() + Send + 'static,
        additional_receivers: Vec<Receiver<Message>>,
    ) -> ActorThread {
        let name = collection.name();

        let (message_sender, dispatch_receiver) = channel::<Arc<Message>>();
        let (kill_sender, kill_receiver) = channel::<()>();
        let filter = collection.filter().compile();

        let dispatch_sender = message_sender.clone();
        let handle = thread::Builder::new()
            .name(format!("actors={}", name))
            .spawn(move || {
                let tokio = tokio::runtime::Builder::new_current_thread()
                    .build()
                    .map_err(WaefError::new)
                    .unwrap();

                let _tokioguard = tokio.enter();

                barrier.wait();
                Self::run_collection(
                    collection,
                    filter,
                    kill_receiver,
                    on_tick_callback,
                    on_kill_callback,
                    dispatch_sender,
                    dispatch_receiver,
                    additional_receivers,
                )
            })
            .unwrap();

        ActorThread {
            name,
            handle,
            sender: message_sender,
            killer: kill_sender,
        }
    }
}

fn recv_timeout_hires<T>(
    dispatch_receiver: &Receiver<T>,
    timeout: Duration,
) -> std::result::Result<T, RecvTimeoutError> {
    if timeout < Duration::from_millis(16) {
        let start_time = Instant::now();
        loop {
            match dispatch_receiver.try_recv() {
                Ok(msg) => return Ok(msg),
                Err(TryRecvError::Empty) if start_time.elapsed() > timeout => {
                    return Err(RecvTimeoutError::Timeout)
                }
                Err(TryRecvError::Empty) => std::thread::sleep(Duration::from_millis(1)),
                Err(TryRecvError::Disconnected) => return Err(RecvTimeoutError::Disconnected),
            }
        }
    } else {
        dispatch_receiver.recv_timeout(timeout)
    }
}

impl ActorThread {
    pub fn sender(&self) -> &Sender<Arc<Message>> {
        &self.sender
    }

    pub fn killer(&self) -> &Sender<()> {
        &self.killer
    }

    pub fn dispatch(&self, msg: Arc<Message>) {
        _ = self.sender.send(msg);
    }

    pub fn kill(&self) {
        _ = self.killer.send(());
    }

    pub fn join(self) -> Result<()> {
        match self.handle.join() {
            Ok(_) => Ok(()),
            Err(_) => Err(WaefError::new("Failed to join")),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::actors::thread::recv_timeout_hires;
    use std::{
        sync::mpsc::channel,
        time::{Duration, Instant},
    };

    #[test]
    fn testing_recv_timeout_long() {
        let test_ms = 100;

        let start_time = Instant::now();
        let (_sender, receiver) = channel::<()>();
        _ = &recv_timeout_hires(&receiver, Duration::from_millis(test_ms));

        let delta = start_time.elapsed();
        assert!(
            delta >= Duration::from_millis(test_ms),
            "Expected delta of at least {test_ms}ms but was {:?}",
            delta
        );
        assert!(
            delta < Duration::from_millis(test_ms + 16),
            "Expected delta of at most {}ms but was {:?}",
            test_ms + 16,
            delta
        );
    }

    #[test]
    fn testing_recv_timeout_short() {
        let test_ms = 10;
        let start_time = Instant::now();
        let (_sender, receiver) = channel::<()>();
        _ = &recv_timeout_hires(&receiver, Duration::from_millis(test_ms));

        let delta = start_time.elapsed();
        assert!(
            delta >= Duration::from_millis(test_ms),
            "Expected delta of at least {test_ms}ms but was {:?}",
            delta
        );
        assert!(
            delta < Duration::from_millis(test_ms + 5),
            "Expected delta of at most {}ms but was {:?}",
            test_ms + 5,
            delta
        );
    }

    #[test]
    fn testing_recv_timeout_zero() {
        let start_time = Instant::now();
        let (_sender, receiver) = channel::<()>();
        _ = &recv_timeout_hires(&receiver, Duration::ZERO);

        let delta = start_time.elapsed();
        assert!(
            delta < Duration::from_millis(1),
            "Expected delta of at most {}ms but was {:?}",
            5,
            delta
        );
    }
}
