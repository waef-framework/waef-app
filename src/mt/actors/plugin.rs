use std::{
    cmp::max,
    sync::mpsc::{channel, Receiver, Sender},
    thread,
};

use waef_core::{
    actors::{ActorsInjector, ActorsPlugin, IsActor, UseActors},
    core::{ExecutionResult, Message, WaefError},
    IsTimer,
};

use crate::application::{Application, ApplicationPlugin};

use super::runtime::ActorRuntimeBuilder;

pub struct UseActorsPlugin {
    plugins: Vec<Box<dyn FnOnce(Box<Self>) -> Box<Self>>>,
    actors: Vec<Box<dyn IsActor>>,
    timers: Vec<Box<dyn Fn(Sender<Message>) -> Box<dyn IsTimer>>>,
    receiver: Receiver<Message>,
    processing_thread_count: usize,
    priority_thread_count: usize,
    io_thread_count: usize,
    terminate: (Sender<()>, Receiver<()>),
}

impl UseActorsPlugin {
    pub fn new(receiver: Receiver<Message>) -> Self {
        let total_available_threads = thread::available_parallelism()
            .map(|i| i.get())
            .unwrap_or(1) as isize;

        let io_thread_count: usize = 1;
        let priority_thread_count: usize = if total_available_threads > 16 {
            4
        } else if total_available_threads > 8 {
            2
        } else {
            1
        };
        let processing_thread_count = max(
            1,
            total_available_threads - (priority_thread_count + io_thread_count) as isize,
        ) as usize;

        Self {
            plugins: vec![],
            actors: vec![],
            timers: vec![],
            receiver,
            io_thread_count,
            priority_thread_count,
            processing_thread_count,
            terminate: channel(),
        }
    }

    pub fn with_io_thread_count(self, io_thread_count: usize) -> Self {
        Self {
            io_thread_count: if io_thread_count != 0 {
                io_thread_count
            } else {
                self.io_thread_count
            },
            ..self
        }
    }

    pub fn try_with_io_thread_count(self, io_thread_count: Option<usize>) -> Self {
        Self {
            io_thread_count: io_thread_count
                .filter(|f| *f != 0)
                .unwrap_or(self.io_thread_count),
            ..self
        }
    }

    pub fn with_processing_thread_count(self, processing_thread_count: usize) -> Self {
        Self {
            processing_thread_count: if processing_thread_count != 0 {
                processing_thread_count
            } else {
                self.processing_thread_count
            },
            ..self
        }
    }

    pub fn try_with_processing_thread_count(self, processing_thread_count: Option<usize>) -> Self {
        Self {
            processing_thread_count: processing_thread_count
                .filter(|f| *f != 0)
                .unwrap_or(self.processing_thread_count),
            ..self
        }
    }

    pub fn with_priority_thread_count(self, priority_thread_count: usize) -> Self {
        Self {
            priority_thread_count: if priority_thread_count != 0 {
                priority_thread_count
            } else {
                self.priority_thread_count
            },
            ..self
        }
    }

    pub fn try_with_priority_thread_count(self, priority_thread_count: Option<usize>) -> Self {
        Self {
            priority_thread_count: priority_thread_count
                .filter(|f| *f != 0)
                .unwrap_or(self.priority_thread_count),
            ..self
        }
    }

    pub fn with_timer(
        mut self,
        factory: impl Fn(Sender<Message>) -> Box<dyn IsTimer> + 'static,
    ) -> Self {
        self.timers.push(Box::new(factory));
        self
    }
}

impl UseActors for UseActorsPlugin {
    fn use_actor(mut self, actor: Box<dyn IsActor>) -> Self {
        self.actors.push(actor);
        self
    }
}
impl UseActors for Box<UseActorsPlugin> {
    fn use_actor(mut self, actor: Box<dyn IsActor>) -> Self {
        self.actors.push(actor);
        self
    }
}

impl ApplicationPlugin for UseActorsPlugin {
    fn apply(mut self: Box<Self>, app: &mut Application) {
        let mut actor_builder = ActorRuntimeBuilder::new();
        let plugins: Vec<Box<dyn FnOnce(Box<Self>) -> Box<Self>>> =
            self.plugins.drain(..).collect();
        self = plugins
            .into_iter()
            .fold(self, |this, plugin| (plugin)(this));
        for actor in self.actors {
            actor_builder = actor_builder.add_box(actor);
        }

        let actor_runtime = actor_builder.start(
            self.priority_thread_count,
            self.processing_thread_count,
            self.io_thread_count,
            self.receiver,
            self.terminate.0,
            self.timers,
        );

        let app_terminate = self.terminate.1;
        app.with_on_tick(move || {
            let _span = tracing::trace_span!("use_actors::tick").entered();

            if let Ok(()) = app_terminate.try_recv() {
                Ok(ExecutionResult::Kill)
            } else {
                Ok(ExecutionResult::NoOp)
            }
        });
        app.with_cleanup(move || {
            let _span = tracing::trace_span!("use_actors::cleanup").entered();

            _ = actor_runtime.0.send(());

            match actor_runtime.1.join() {
                Ok(result) => result,
                Err(_) => Err(WaefError::new("Failed to join onto actor runtime thread")),
            }
        });
    }
}
impl UseActorsPlugin {
    pub fn use_plugin(mut self, plugin: impl ActorsPlugin + 'static) -> UseActorsPlugin {
        self.plugins.push(Box::new(move |this| plugin.apply(this)));
        self
    }

    pub fn use_inject(self, plugin: &impl ActorsInjector) -> UseActorsPlugin {
        plugin.inject(self)
    }
}
