use std::collections::HashMap;
use std::sync::Mutex;

use waef_core::resources::ResourceStore;

pub struct LockingResourceStore {
    data: Mutex<HashMap<u128, Vec<u8>>>,
}
impl LockingResourceStore {
    pub fn new() -> Self {
        Self {
            data: Mutex::new(HashMap::new()),
        }
    }
}
impl ResourceStore for LockingResourceStore {
    fn get_size(&self, key: u128) -> Option<usize> {
        let _span = tracing::trace_span!("LockingResourceStore::get_size", key).entered();

        match self.data.lock() {
            Ok(store) => store.get(&key).map(|d| d.len()),
            _ => None,
        }
    }

    fn remove(&self, key: u128) -> Option<Box<[u8]>> {
        let _span = tracing::trace_span!("LockingResourceStore::remove", key).entered();

        match self.data.lock() {
            Ok(mut store) => store.remove(&key).map(|d| d.into_boxed_slice()),
            _ => None,
        }
    }

    fn put(&self, key: u128, data: &[u8]) {
        let _span = tracing::trace_span!("LockingResourceStore::put", key).entered();

        match self.data.lock() {
            Ok(mut store) => {
                if let Some(buffer) = store.get_mut(&key) {
                    if buffer.len() != data.len() {
                        buffer.resize(data.len(), 0);
                    }
                    buffer.copy_from_slice(data);
                } else {
                    let mut item = vec![0u8; data.len()];
                    item.copy_from_slice(data);
                    store.insert(key, item);
                }
            }
            _ => (),
        }
    }

    fn get(&self, key: u128, buffer: &mut [u8]) -> Option<usize> {
        let _span = tracing::trace_span!("LockingResourceStore::get", key).entered();

        match self.data.lock() {
            Ok(store) => {
                if let Some(data) = store.get(&key) {
                    let target_data_len = data.len().min(buffer.len());
                    buffer[..target_data_len].copy_from_slice(&data[..target_data_len]);
                    Some(target_data_len)
                } else {
                    None
                }
            }
            _ => None,
        }
    }

    fn put_slice(&self, key: u128, start_index: usize, data: &[u8]) {
        let _span = tracing::trace_span!("LockingResourceStore::put_slice", key).entered();

        match self.data.lock() {
            Ok(mut store) => {
                if let Some(buffer) = store.get_mut(&key) {
                    let target_buffer_size = start_index + data.len();
                    if buffer.len() < target_buffer_size {
                        buffer.extend(vec![0u8; target_buffer_size - buffer.len()]);
                    }
                    buffer[start_index..target_buffer_size].copy_from_slice(data);
                } else {
                    let mut item = vec![0u8; start_index + data.len()];
                    item[start_index..start_index + data.len()].copy_from_slice(data);
                    store.insert(key, item);
                }
            }
            _ => (),
        }
    }

    fn get_slice(&self, key: u128, start_index: usize, buffer: &mut [u8]) -> Option<usize> {
        let _span = tracing::trace_span!("LockingResourceStore::get_slice", key).entered();

        match self.data.lock() {
            Ok(store) => {
                if let Some(data) = store.get(&key) {
                    let target_data_end = start_index + data.len();
                    let target_data_range = if data.len() < target_data_end {
                        start_index..data.len()
                    } else {
                        start_index..target_data_end
                    };
                    let read_len = target_data_range.len();
                    buffer.copy_from_slice(&data[target_data_range]);
                    Some(read_len)
                } else {
                    None
                }
            }
            _ => None,
        }
    }

    fn get_cloned(&self, key: u128) -> Option<Vec<u8>> {
        let _span = tracing::trace_span!("LockingResourceStore::get_cloned", key).entered();

        match self.data.lock() {
            Ok(store) => {
                if let Some(data) = store.get(&key) {
                    Some(data.clone())
                } else {
                    None
                }
            }
            _ => None,
        }
    }
}
